Thanks for downloading DATAWIN Lite. You will need a few libraries on Linux:
  sudo apt-get install libopenal1  libalure1

You will need my custom branch of ENIGMA to download this (use the ld32 tag):
  https://github.com/sorlok/enigma-dev

If you get library issues, you can probably "apt-get" what you need.

Please post in the comments if you find any bugs.

NOTE: You *must* compile in Debug mode, due to a weird interaction between gcc and ENIGMA.

NOTE: The EGM file *is* the source; it's a zip file, and can also be opened directly by ENIGMA.